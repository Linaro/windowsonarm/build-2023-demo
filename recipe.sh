#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

get_python()
{
    wget -q https://www.nuget.org/api/v2/package/pythonarm64/3.11.3 -O python.zip
    unzip -q python.zip
    export PATH=$(pwd)/tools:$PATH
    which python
}

get_dependencies()
{
    python -m pip install --upgrade pip setuptools
    python -m pip install numpy pandas pillow matplotlib scikit-build
    python -m pip install --no-build-isolation opencv-python
}

get_python
get_dependencies

